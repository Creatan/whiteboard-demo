/**
* Whiteboard class for managing our multi-user whiteboard
* @constructor
* @param {string} elementId - id of the canvas element in our html
*/
function Whiteboard(elementId){
  this.canvas = document.getElementById(elementId);
  this.context = this.canvas.getContext('2d');
  this.isDrawing = false;
  this.state = [];
  this.context.strokeStyle = '#000000';
  this.context.lineWidth = 10;
  this.context.lineJoin = 'round';
  this.context.lineCap = 'round';
};

/**
* our applications initialisation code
*/
Whiteboard.prototype.init = function init(){
  // set our drawing canvas to occupy the full viewport
  this.canvas.setAttribute('width',window.innerWidth);
  this.canvas.setAttribute('height',window.innerHeight);

  this.addListeners();
  this.fetchState();
};

/**
* adds needed eventlisteners
*/
Whiteboard.prototype.addListeners = function addListeners () {
  var _this = this;
  this.canvas.addEventListener('mousedown',function(e){
    console.debug("Start drawing:",e.clientX,e.clientY);
    _this.isDrawing = true;
    _this.context.beginPath();
    _this.moveTo(e.clientX,e.clientY);
  });
  this.canvas.addEventListener('mousemove',function(e){
    if(_this.isDrawing){
      console.debug("Drawing:",e.clientX,e.clientY);
      _this.draw(e.clientX,e.clientY);
    }
  });
  this.canvas.addEventListener('mouseup',function(e){
    console.debug('Stopped drawing:',e.clientX,e.clientY);
    _this.isDrawing = false;
  });
  this.canvas.addEventListener('mouseout',function(e){
    if(_this.isDrawing){
        console.debug('Out of canvas;:',e.clientX,e.clientY);
        _this.isDrawing = false;
    }
  });
};

/**
* returns current application state
* @return {array} state
*/
Whiteboard.prototype.getState = function getState() {
  return this.state;
};

/*
* fetches the state from server
*/
Whiteboard.prototype.fetchState = function fetchState() {
    // xmlhttprequest
    //set state to reflect current situation
    this.state = [];
};
/**
* Draws the path we are tracing
*/
Whiteboard.prototype.draw = function draw(targetX,targetY) {
    this.context.lineTo(targetX,targetY);
    this.context.stroke();
};
// eventlistener to execute our code after content is loaded
// similar effect to jquery's document.ready
// supported by major browsers
document.addEventListener("DOMContentLoaded",function(event){
  var socket = io();
  var whiteboard = new Whiteboard('whiteboard');
  whiteboard.init();
});
