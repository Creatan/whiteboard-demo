/**
 * Whiteboard class for managing our multi-user whiteboard
 * @constructor
 * @param {string} elementId - id of the canvas element in our html
 * @param {object} socket - our socket connection for websockets
 */
function Whiteboard(elementId, socket) {
  this.canvas = document.getElementById(elementId);
  this.context = this.canvas.getContext('2d');

  // isDrawing handles if mousemove should draw or not
  this.isDrawing = false;
  this.socket = socket;
  this.color = "#000000";
  this.context.strokeStyle = this.color;
  this.context.lineWidth = 10;
  this.context.lineJoin = 'round';
  this.context.lineCap = 'round';
};

/**
 * our applications initialisation code
 */
Whiteboard.prototype.init = function init() {
  // set our drawing canvas to occupy the full viewport
  this.canvas.setAttribute('width', window.innerWidth);
  this.canvas.setAttribute('height', window.innerHeight - this.canvas.offsetTop);

  this.addListeners();
};

/**
 * adds needed eventlisteners
 */
Whiteboard.prototype.addListeners = function addListeners() {
  var _this = this;
  this.canvas.addEventListener('mousedown', function(e) {
    _this.isDrawing = true;
    _this.context.beginPath();
    _this.context.moveTo(e.clientX, e.clientY - _this.canvas.offsetTop);
    _this.socket.emit('start-drawing', {
      X: e.clientX,
      Y: e.clientY,
      color: _this.color
    });
  });
  this.canvas.addEventListener('mousemove', function(e) {
    if (_this.isDrawing) {
      _this.draw(e.clientX, e.clientY, _this.color);
      _this.socket.emit('drawing', {
        X: e.clientX,
        Y: e.clientY,
        color: _this.color
      });
    }
  });

  /*
   *  When we stop drawing either by bringing our mouse button up
   *  or moving outside the canvas we send the current canvas to the
   *  server as data uri.
   */
  this.canvas.addEventListener('mouseup', function(e) {
    _this.isDrawing = false;
    _this.socket.emit('stop-drawing', _this.canvas.toDataURL());
  });
  this.canvas.addEventListener('mouseout', function(e) {
    if (_this.isDrawing) {
      _this.isDrawing = false;
      _this.socket.emit('stop-drawing', _this.canvas.toDataURL());
    }
  });

  //listen to our color input and change stroke color
  document.getElementById('color').addEventListener('change', function() {
    var color = this.value;
    if (color.charAt(0) !== '#') {
      color = '#' + color;
    }
    if (color.length > 7) {
      color = color.substring(0, 7);
    }
    _this.color = color;
    this.value = color;
  });
};

/**
 * Draws the path we are tracing
 */
Whiteboard.prototype.draw = function draw(targetX, targetY, color) {
  if (typeof color !== 'undefined') {
    this.context.strokeStyle = color;
  }
  this.context.lineTo(targetX, targetY - this.canvas.offsetTop);
  this.context.stroke();
};
/**
 * Allows access to our canvas' context
 * @return {object} context
 */
Whiteboard.prototype.getContext = function getContext() {
  return this.context;
};

// eventlistener to execute our code after content is loaded
// similar effect to jquery's document.ready
// supported by major browsers
document.addEventListener("DOMContentLoaded", function(event) {
  // connect to our server via websocket
  var connected = false;
  var socket = io();

  // initialise our whiteboard application
  var whiteboard = new Whiteboard('whiteboard', socket);
  whiteboard.init();

  // drawing other users coordinates that are broadcasted via
  // websocket connection
  socket.on('start-drawing', function(coords) {
    var context = whiteboard.getContext();
    context.beginPath();
    context.moveTo(coords.X, coords.Y - context.canvas.offsetTop);
  });

  socket.on('drawing', function(coords) {
    whiteboard.draw(coords.X, coords.Y, coords.color);
  });

  //update our user counter
  socket.on('users', function(userCount) {
    document.getElementById('counter').innerHTML = userCount;
  });

  socket.on('history', function(imageData) {
    var context = whiteboard.getContext(),
      canvas = context.canvas;
    // we need to turn the raw image data into an html image element before
    // we can use the drawImage-funtion.
    var img = new Image(canvas.width, canvas.height);
    img.src = imageData;
    // ensure that the image has been loaded before adding it to the canvas
    img.onload = function() {
      context.drawImage(img, 0, 0, canvas.width, canvas.height, 0, 0, canvas.width, canvas.height);
    }
  });
});
