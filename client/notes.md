Hi,

In this email is attached an solution to your test. I had a lot of fun with working on it. Before this I hadn't really played around with html canvas before so this was also a huge learning experience. There are a lot of things that I'd like to improve on the solution. Like handling the resizing of viewport or what happens when there are canvases that are different sizes. Also having some sort of tools for drawing basic shapes and writing text would be an huge usability improvement. 


The application requires nodejs to run. You can install other dependencies by running "npm install" in the idt-whiteboard folder. This should install express and socket.io modules. To run the server use "node server/app.js" in the idt-whiteboard folder.
