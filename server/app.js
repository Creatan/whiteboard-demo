var express = require('express');
var app = express();
var http = require('http').Server(app);
var path = require('path');
var io = require('socket.io')(http);

// static mapping for serving client files
app.use('/static', express.static(__dirname + "/../client"));

app.get('/', function(req, res) {
  res.sendFile(path.resolve(__dirname + '/index.html'));
});

// store the history as an image that is drawn when new user connects
var history = '';
// keep count on connected users
var users = 0;
// track our clients so we can send history to only new connections
var clients = [];

// websockets with socket.io
// broadcasting the drawn coordinates to other connected users
io.on('connection', function(socket) {
  console.log('an user connected');
  users++;

  // save the client id and send history only if
  // this is a new connection and there's something to send
  if (clients.indexOf(socket.id) === -1) {
    clients.push(socket.id);
    if (history !== '') {
      socket.emit('history', history);
    }

  }

  // broadcast user count when new connection is made
  io.emit('users', users);

  socket.on('disconnect', function() {
    users--;
    console.log('user disconneced');
    io.emit('users', users);
  });

  socket.on('start-drawing', function(coords) {
    socket.broadcast.emit('start-drawing', coords);
  });

  socket.on('drawing', function(coords) {
    socket.broadcast.emit('drawing', coords);
  });

  // when user stops drawing we take a snapshot of the current whiteboard
  // we can then serve this to new users when they connect
  socket.on('stop-drawing', function(imageData) {
    console.log('saved image data');
    history = imageData;
  });
});

// starting our server
http.listen(8000, function() {
  console.log('listening on *:8000');
});
